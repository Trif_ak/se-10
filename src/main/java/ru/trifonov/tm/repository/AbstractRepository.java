package ru.trifonov.tm.repository;

import java.util.LinkedHashMap;
import java.util.Map;

abstract class AbstractRepository<T> {
    final Map<String, T> entities = new LinkedHashMap<>();
}
