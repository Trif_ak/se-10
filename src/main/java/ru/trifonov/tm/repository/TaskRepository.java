package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.ITaskRepository;
import ru.trifonov.tm.entity.Task;
import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    private final Iterator<Map.Entry<String, Task>> entryIterator = entities.entrySet().iterator();

    @Override
    public void persist(@NotNull final Task task) {
        entities.put(task.getId(), task);
    }

    @Override
    public void merge(@NotNull final Task task) {
        if (entities.containsKey(task.getId())) {
            update(task.getName(), task.getId(), task.getProjectId(), task.getUserId(), task.getDescription(), task.getBeginDate(), task.getEndDate());
        } else
            insert(task.getName(), task.getId(), task.getProjectId(), task.getUserId(), task.getDescription(), task.getBeginDate(), task.getEndDate());
    }

    @Override
    public void insert(@NotNull final String name, @NotNull final String id, @NotNull final String projectId, @NotNull final String userId, @NotNull final String description, @NotNull final Date beginDate, @NotNull final Date endDate) {
        @NotNull final Task task = new Task(name, id, projectId, userId, description, beginDate, endDate);
        entities.put(id, task);
    }

    @Override
    public void update(@NotNull final String name, @NotNull final String id, @NotNull final String projectId, @NotNull final String userId, @NotNull final String description, @NotNull final Date beginDate, @NotNull final Date endDate) {
        @NotNull final Task task = new Task(name, id, projectId, userId, description, beginDate, endDate);
        entities.put(id, task);
    }

    @Override
    public Task findOne(@NotNull final String id, @NotNull final String userId) {
        @NotNull final Task task = entities.get(id);
        if (task == null) throw new NullPointerException("Not found task. Enter correct date");
        if (!task.getUserId().equals(userId)) throw new IllegalStateException("Not found task. Enter correct date");
        return task;
    }

    @Override
    public List<Task> findAll(@NotNull final String projectId, @NotNull final String userId) {
        @NotNull final List<Task> output = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Task> task : entities.entrySet()) {
            if (task.getValue().getProjectId().equals(projectId)) {
                output.add(task.getValue());
            }
            if (output.isEmpty()) throw new NullPointerException("Not found tasks. Enter correct date");
        }
        return output;
    }

    @Override
    public void remove(@NotNull final String id, @NotNull final String userId) {
        while (entryIterator.hasNext()) {
            @NotNull final Map.Entry<String, Task> projectEntry = entryIterator.next();
            if (projectEntry.getKey().equals(id) && projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
                break;
            }
        }
    }

    @Override
    public void removeAllOfProject(@NotNull final String projectId, @NotNull final String userId) {
        while (entryIterator.hasNext()) {
            @NotNull final Map.Entry<String, Task> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getProjectId().equals(projectId) && projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
            }
        }
    }

    @Override
    public void removeAllOfUser(@NotNull final String userId) {
        while (entryIterator.hasNext()) {
            @NotNull final Map.Entry<String, Task> projectEntry = entryIterator.next();
            if (projectEntry.getValue().getUserId().equals(userId)) {
                entryIterator.remove();
            }
        }
    }

    @Override
    public List<Task> findByPartString(@NotNull final String userId, @NotNull final String projectId, @NotNull final String partString) {
        @Nullable final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Task> project : entities.entrySet()) {
            if (project.getValue().getUserId().equals(userId) && project.getValue().getProjectId().equals(projectId)){
                tasks.add(project.getValue());
            }
        }
        if (tasks.isEmpty()) throw new NullPointerException("Not found tasks. Enter correct date");

        @Nullable final List<Task> output = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (task.getName().contains(partString) || task.getDescription().contains(partString)) {
                output.add(task);
            }
        }
        if (output.isEmpty()) throw new NullPointerException("Not found tasks. Enter correct date");
        return output;
    }

    @Override
    public List<Task> findAll() {
        @NotNull final List<Task> output = new ArrayList<>(entities.values());
//        if (output.isEmpty()) throw new NullPointerException("Not found tasks. Create some tasks");
        return output;
    }
}