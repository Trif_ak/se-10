package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.IProjectRepository;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.util.IdUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {
    @NotNull private IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) throw new NullPointerException("Enter correct data");
        projectRepository.persist(project);
    }
    @Override
    public void insert(@Nullable final String name, @Nullable final String userId, @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Project project = new Project(name, IdUtil.getUUID(), userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        projectRepository.persist(project);
    }

    @Override
    public void update(@Nullable final String name, @Nullable final String id, @Nullable final String userId, @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final Project project = new Project(name, IdUtil.getUUID(), userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        projectRepository.merge(project);
    }

    @Override
    public Project findOne(@Nullable final String id, @Nullable final String userId) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.findOne(id, userId);
    }

    @Override
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.findAll(userId);
    }

    @Override
    public void removeOne(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        projectRepository.remove(id, userId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        projectRepository.removeAll(userId);
    }

    @Override
    public List<Project> sortBy(@Nullable final String userId, @Nullable final String comparatorName) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (comparatorName == null || comparatorName.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final List<Project> projects = findAll(userId);
        if (projects == null || projects.isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final Comparator comparator = getComparator(comparatorName);
        if (comparator == null) throw new NullPointerException("Enter correct data");
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> findByPartString(@Nullable final String userId, @Nullable final String partString) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (partString == null || partString.isEmpty()) throw new NullPointerException("Enter correct data");
        return projectRepository.findByPartString(userId, partString);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }
}