package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.ITaskRepository;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.util.IdUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractService implements ITaskService {
    private @NotNull ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) throw new NullPointerException("Enter correct data");
        taskRepository.persist(task);
    }

    @Override
    public void init(@Nullable final String name, @Nullable final String projectId, @Nullable final String userId, @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final Task task = new Task(name, IdUtil.getUUID(), projectId, userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        taskRepository.persist(task);
    }

    @Override
    public void update(@Nullable final String name, @Nullable final String id, @Nullable final String projectId, @Nullable final String userId, @Nullable final String description, @Nullable final String beginDate, @Nullable final String endDate) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final Task task = new Task(name, IdUtil.getUUID(), projectId, userId, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        taskRepository.merge(task);
    }

    @Override
    public Task findOne(@Nullable final String id, @Nullable final String userId) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return taskRepository.findOne(id, userId);
    }

    @Override
    public List<Task> findAll(@Nullable final String projectId, @Nullable final String userId) throws Exception {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return taskRepository.findAll(projectId, userId);
    }

    @Override
    public void remove(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.remove(id, userId);
    }

    @Override
    public void removeAllOfProject(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.removeAllOfProject(projectId, userId);
    }

    @Override
    public void removeAllOfUser(@Nullable final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        taskRepository.removeAllOfUser(userId);
    }

    @Override
    public List<Task> sortBy(@Nullable final String projectId, @Nullable final String userId, @Nullable final String comparatorName) throws Exception {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (comparatorName == null || comparatorName.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final List<Task> tasks = findAll(projectId, userId);
        if (tasks == null || tasks.isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final Comparator comparator = getComparator(comparatorName);
        if (comparator == null) throw new NullPointerException("Enter correct data");
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> findByPartString(@Nullable final String userId, @Nullable final String projectId, @Nullable final String partString) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (partString == null || partString.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return taskRepository.findByPartString(userId, projectId, partString);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }
}