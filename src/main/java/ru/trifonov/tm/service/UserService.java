package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.IUserRepository;
import ru.trifonov.tm.api.IUserService;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.util.HashUtil;
import ru.trifonov.tm.util.IdUtil;

import java.util.Collection;
import java.util.List;

public final class UserService extends AbstractService implements IUserService {
    @NotNull private IUserRepository userRepository;

    public UserService(@NotNull IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if (user == null) throw new NullPointerException("Enter correct data");
        userRepository.persist(user);
    }

    @Override
    public User authorizationUser(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return userRepository.findPassword(userRepository.findLogin(login), HashUtil.md5(password));
    }

    @Override
    public String registrationUser(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userRepository.findLogin(login) != null) throw new NullPointerException("Enter correct data");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.REGULAR_USER);
        userRepository.persist(user);
        return user.getLogin();
    }

    @Override
    public String registrationAdmin(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userRepository.findLogin(login) != null) throw new NullPointerException("Enter correct data");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.ADMIN);
        userRepository.persist(user);
        return user.getLogin();
    }

    @Override
    public void update(@Nullable final String id, @Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (roleType == null) throw new NullPointerException("Enter correct data");
        @NotNull final User user = new User(IdUtil.getUUID(), login, HashUtil.md5(password), RoleType.ADMIN);
        userRepository.merge(user);
    }

    @Override
    public List<User> findAll() throws Exception {
        return userRepository.findAll();
    }

    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        return userRepository.findOne(id);
    }

    @Override
    public void removeOne(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        userRepository.removeOne(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }
}