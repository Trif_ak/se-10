package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.comparator.DateBeginComparator;
import ru.trifonov.tm.comparator.DateCreateComparator;
import ru.trifonov.tm.comparator.DateEndComparator;
import ru.trifonov.tm.comparator.StatusComparator;
import ru.trifonov.tm.entity.AbstractComparableEntity;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

abstract class AbstractService {
    @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    @NotNull private static final Map<String, Comparator<AbstractComparableEntity>> comparators = new LinkedHashMap<>();
    @NotNull private static final Comparator<AbstractComparableEntity> dateBeginComparator = new DateBeginComparator();
    @NotNull private static final Comparator<AbstractComparableEntity> dateEndComparator = new DateEndComparator();
    @NotNull private static final Comparator<AbstractComparableEntity> dateCreateComparator = new DateCreateComparator();
    @NotNull private static final Comparator<AbstractComparableEntity> statusComparator = new StatusComparator();

    static {
        comparators.put("date-create", dateCreateComparator);
        comparators.put("date-begin", dateBeginComparator);
        comparators.put("date-end", dateEndComparator);
        comparators.put("status", statusComparator);
    }

    Comparator<AbstractComparableEntity> getComparator(@NotNull final String comparatorName) {
        return comparators.get(comparatorName);
    }
}
