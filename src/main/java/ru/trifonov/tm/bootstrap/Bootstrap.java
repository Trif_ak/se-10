package ru.trifonov.tm.bootstrap;

import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.*;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.repository.*;
import ru.trifonov.tm.service.*;
import ru.trifonov.tm.command.AbstractCommand;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    @Nullable private User currentUser = new User();
    @Nullable private Collection<RoleType> roleTypes;
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final Scanner inCommand = new Scanner(System.in);
    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(@NotNull final Set<Class<? extends AbstractCommand>> commandsClass) throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        BasicConfigurator.configure();
        for (@NotNull final Class clazz : commandsClass) {
                registry(clazz);
            }
            start();
    }

    private void registry(@NotNull final Class clazz) throws Exception {
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setServiceLocator(this);
        @NotNull final String nameCommand = abstractCommand.getName();
        @NotNull final String descriptionCommand = abstractCommand.getDescription();
        commands.put(nameCommand, abstractCommand);
    }

    private void start() {
        try {
            userService.registrationAdmin("admin", "admin");
            userService.registrationUser("user", "user");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        System.out.println("*** WELCOME TO TASK MANAGER *** \n Enter command \"help\" for watch all COMMANDS");
        while (true) {
            try {
                System.out.println("\n Enter command:");
                @NotNull final AbstractCommand abstractCommand = commands.get(inCommand.nextLine());
                if (abstractCommand == null) {
                    System.out.println("There is no such command");
                    continue;
                }
                execute(abstractCommand, currentUser);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void execute (@Nullable final AbstractCommand abstractCommand, @Nullable final User currentUser) throws Exception {
        if (abstractCommand.roleType() != null) {
            roleTypes = Arrays.asList(abstractCommand.roleType());
            if (roleTypes.contains(currentUser.getRoleType())) abstractCommand.execute();
            else System.out.println("This command is not available");
        }
        else abstractCommand.execute();
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public Scanner getInCommand() {
        return inCommand;
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Override
    public String getCurrentUserID() {
        return currentUser.getId();
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}


