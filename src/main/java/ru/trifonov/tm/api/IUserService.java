package ru.trifonov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public interface IUserService {
    void persist(@Nullable User user) throws Exception;
    User authorizationUser(@Nullable String login, @Nullable String password) throws Exception;
    String registrationUser(@Nullable String login, @Nullable String password) throws Exception;
    String registrationAdmin(@Nullable String login, @Nullable String password) throws Exception;
    void update(@Nullable String id, @Nullable String login, @Nullable String password, @Nullable RoleType roleType) throws Exception;
    List<User> findAll() throws Exception;
    User findOne(@Nullable String id) throws Exception;
    void removeOne(@Nullable String id);
    void removeAll();
}
