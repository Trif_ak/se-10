package ru.trifonov.tm.api;

import org.jetbrains.annotations.Nullable;

import ru.trifonov.tm.entity.Project;


import java.util.Collection;
import java.util.List;

public interface IProjectService {
    void persist(@Nullable Project project);
    void insert(@Nullable String name, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    void update(@Nullable String name, @Nullable String id, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    Project findOne(@Nullable String id, @Nullable String userId) throws Exception;
    List<Project> findAll(@Nullable String userId) throws Exception;
    void removeOne(@Nullable String id, @Nullable String userId) throws Exception;
    void removeAll(@Nullable String userId) throws Exception;
    List<Project> sortBy(@Nullable String userId, @Nullable String comparatorName) throws Exception;
    List<Project> findByPartString(@Nullable String userId, @Nullable String partString) throws Exception;
    List<Project> findAll();
}
