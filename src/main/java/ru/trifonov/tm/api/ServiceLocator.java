package ru.trifonov.tm.api;

import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.User;

import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {
    IProjectService getProjectService();
    ITaskService getTaskService();
    IUserService getUserService();
    Scanner getInCommand();
    Map<String, AbstractCommand> getCommands();
    String getCurrentUserID();
    User getCurrentUser();
    void setCurrentUser(User currentUser);
}
