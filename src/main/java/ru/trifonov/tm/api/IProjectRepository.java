package ru.trifonov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectRepository {
    void merge(@NotNull Project project);
    void persist(@NotNull Project project);
    void insert(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate);
    void update(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate);
    Project findOne(@NotNull String id, @NotNull String userId) throws Exception;
    List<Project> findAll(@NotNull String userId) throws Exception;
    void remove(@NotNull String id, @NotNull String userId);
    void removeAll(@NotNull String userId);
    List<Project> findByPartString(@NotNull String userId, @NotNull String partString) throws Exception;
    List<Project> findAll();
}
