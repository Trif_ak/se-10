package ru.trifonov.tm.api;

import ru.trifonov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface ITaskService {
    void persist(@Nullable Task task);
    void init(@Nullable String name, @Nullable String projectId, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    void update(@Nullable String name, @Nullable String id, @Nullable String projectId, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    Task findOne(@Nullable String id, @Nullable String userId) throws Exception;
    List<Task> findAll(@Nullable String projectId, @Nullable String userId) throws Exception;
    void remove(@Nullable String id, @Nullable String userId);
    void removeAllOfProject(@Nullable String projectId, @Nullable String userId);
    void removeAllOfUser(@Nullable String userId);
    List<Task> sortBy(@Nullable String projectId, @Nullable String userId, @Nullable String comparatorName) throws Exception;
    List<Task> findByPartString(@NotNull String userId, @NotNull String projectId, @NotNull String partString);
    List<Task> findAll();
}
