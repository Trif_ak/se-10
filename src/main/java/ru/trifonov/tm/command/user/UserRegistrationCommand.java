package ru.trifonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserRegistrationCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-reg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": registration new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRATION]");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = serviceLocator.getInCommand().nextLine();
        System.out.println("USER " + serviceLocator.getUserService().registrationUser(login, password) + " is registered");
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return null;
    }
}
