package ru.trifonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserAuthorizationCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-auth";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": authorization in program";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER AUTHORIZATION]");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = serviceLocator.getInCommand().nextLine();
        serviceLocator.setCurrentUser(serviceLocator.getUserService().authorizationUser(login, password));
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return null;
    }
}
