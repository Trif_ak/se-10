package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.List;

public final class ProjectFindPartStringCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "project-findPart";
    }

    @Override
    public @NotNull String getDescription() {
        return ": return select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT BY PART TITLE OR DESCRIPTION]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter part of title or description");
        @Nullable final String partString = serviceLocator.getInCommand().nextLine();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findByPartString(userId, partString);
        for (@NotNull final Project project : projects) {
            System.out.println(project);
        }
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
