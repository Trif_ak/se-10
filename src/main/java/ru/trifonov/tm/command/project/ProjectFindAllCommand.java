package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;

public final class ProjectFindAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-findAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND ALL]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        @NotNull final Collection<Project> inputList = serviceLocator.getProjectService().findAll(userId);
        for (@NotNull final Project project : inputList) {
            System.out.println(project);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
