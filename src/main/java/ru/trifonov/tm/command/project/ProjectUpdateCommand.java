package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": update select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT UPDATE]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter the ID of the project you want to update");
        @Nullable final String id = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new name");
        @Nullable final String name = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new description");
        @Nullable final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new begin date. Date format DD.MM.YYYY");
        @Nullable final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new end date. Date format DD.MM.YYYY");
        @Nullable final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getProjectService().update(name, id, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
