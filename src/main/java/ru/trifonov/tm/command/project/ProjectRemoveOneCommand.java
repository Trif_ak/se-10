package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectRemoveOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-removeOne";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": removeOne select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter the ID of the project you want to removeOne");
        @Nullable final String id = serviceLocator.getInCommand().nextLine();
        serviceLocator.getTaskService().removeAllOfProject(id, userId);
        serviceLocator.getProjectService().removeOne(id, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
