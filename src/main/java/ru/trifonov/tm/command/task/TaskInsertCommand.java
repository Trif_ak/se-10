package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskInsertCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-insert";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK INSERT]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter ID of project");
        @Nullable final String projectId = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter name task");
        @Nullable final String name = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter description");
        @Nullable final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter start date task. Date format DD.MM.YYYY");
        @Nullable final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter finish date task. Date format DD.MM.YYYY");
        @Nullable final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getTaskService().init(name, projectId, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
