package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.List;

public class TaskFindPartStringCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "task-findPart";
    }

    @Override
    public @NotNull String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT BY PART TITLE OR DESCRIPTION]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter ID of project");
        @Nullable final String projectId = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter part of title or description");
        @Nullable final String partString = serviceLocator.getInCommand().nextLine();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findByPartString(userId, projectId, partString);
        for (@NotNull final Task task : tasks) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
