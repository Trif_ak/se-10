package ru.trifonov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

import java.io.FileOutputStream;

public final class DomainSaveJacksonJSON extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "save-jac-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": save domain to json with jackson";
    }

    @Override
    public void execute() throws Exception {
        try(@NotNull final FileOutputStream userOut = new FileOutputStream("src/main/files/UserFile.json");
            @NotNull final FileOutputStream taskOut = new FileOutputStream("src/main/files/TaskFile.json");
            @NotNull final FileOutputStream projectOut = new FileOutputStream("src/main/files/ProjectFile.json")
        ){
            System.out.println("[DOMAIN JACKSON JSON]");
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String userJackson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getUserService().findAll());
            userOut.write(userJackson.getBytes());
            userOut.flush();
            @NotNull final String projectJackson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getProjectService().findAll());
            projectOut.write(projectJackson.getBytes());
            projectOut.flush();
            @NotNull final String taskJackson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getTaskService().findAll());
            taskOut.write(taskJackson.getBytes());
            taskOut.flush();
            System.out.println("[OK]");
        }
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
