package ru.trifonov.tm.command.data;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.wrapper.ProjectWrapper;
import ru.trifonov.tm.wrapper.TaskWrapper;
import ru.trifonov.tm.wrapper.UserWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class DomainSaveJaxbJSON extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "save-jax-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": save domain to json with jax-b";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN JAX-B JSON]");
        @NotNull final JAXBContext projectContext = JAXBContext.newInstance(ProjectWrapper.class);
        @NotNull final Marshaller projectMarshaller = projectContext.createMarshaller();
        projectMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        projectMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final ProjectWrapper projectWrapper = new ProjectWrapper();
        projectWrapper.setProjectList(serviceLocator.getProjectService().findAll()) ;
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        projectMarshaller.marshal(projectWrapper, new File("src/main/files/ProjectFile.json"));

        @NotNull final JAXBContext taskContext = JAXBContext.newInstance(TaskWrapper.class);
        @NotNull final Marshaller taskMarshaller = taskContext.createMarshaller();
        taskMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        taskMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final TaskWrapper taskWrapper = new TaskWrapper();
        taskWrapper.setTaskList(serviceLocator.getTaskService().findAll());
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        taskMarshaller.marshal(taskWrapper, new File("src/main/files/TaskFile.json"));

        @NotNull final JAXBContext userContext = JAXBContext.newInstance(UserWrapper.class);
        @NotNull final Marshaller userMarshaller = userContext.createMarshaller();
        userMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        userMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final UserWrapper userWrapper = new UserWrapper();
        userWrapper.setUserList(serviceLocator.getUserService().findAll());
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        userMarshaller.marshal(userWrapper, new File("src/main/files/UserFile.json"));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
