package ru.trifonov.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.io.File;
import java.util.Iterator;

public final class DomainLoadJacksonXML extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "load-jac-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": load domain to xml with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN JACKSON XML]");
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
        @NotNull final Iterator<Project> projectIterator = xmlMapper.readerFor(Project.class).readValues(new File("src/main/files/ProjectFile.xml"));
        while (projectIterator.hasNext()) {
            serviceLocator.getProjectService().persist(projectIterator.next());
        }
        @NotNull final Iterator<Task> taskIterator = xmlMapper.readerFor(Task.class).readValues(new File("src/main/files/TaskFile.xml"));
        while (taskIterator.hasNext()) {
            serviceLocator.getTaskService().persist(taskIterator.next());
        }
        @NotNull final Iterator<User> userIterator = xmlMapper.readerFor(User.class).readValues(new File("src/main/files/UserFile.xml"));
        while (userIterator.hasNext()) {
            serviceLocator.getUserService().persist(userIterator.next());
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
