package ru.trifonov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;

public final class DomainDeserializable extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "deserializable-domain";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": serializable domain";
    }

    @Override
    public void execute() throws Exception {
        try (@NotNull final FileInputStream projectInput = new FileInputStream("src/main/files/ProjectFile");
             @NotNull final FileInputStream taskInput = new FileInputStream("src/main/files/TaskFile");
             @NotNull final FileInputStream userInput = new FileInputStream("src/main/files/UserFile")
        ){
            System.out.println("[DOMAIN SERIALIZABLE]");
            @NotNull final ObjectInputStream userObjectInput = new ObjectInputStream(userInput);
            @Nullable final List<User> userList = (List<User>) userObjectInput.readObject();
            for (@Nullable final User user : userList) {
                serviceLocator.getUserService().persist(user);
            }
            @NotNull final ObjectInputStream taskObjectInput = new ObjectInputStream(taskInput);
            @Nullable final List<Task> taskList = (List<Task>) taskObjectInput.readObject();
            for (@Nullable final Task task : taskList) {
                serviceLocator.getTaskService().persist(task);
            }
            @NotNull final ObjectInputStream projectObjectInput = new ObjectInputStream(projectInput);
            @Nullable final List<Project> projectList = (List<Project>) projectObjectInput.readObject();
            for (@Nullable final Project project : projectList) {
                serviceLocator.getProjectService().persist(project);
            }

            System.out.println("[OK]");
        }
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
