package ru.trifonov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.io.File;
import java.util.Iterator;

public final class DomainLoadJacksonJSON extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "load-jac-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": load domain to json with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN JACKSON XML]");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Iterator<Project> projectIterator = objectMapper.readerFor(Project.class).readValues(new File("src/main/files/ProjectFile.json"));
        while (projectIterator.hasNext()) {
            serviceLocator.getProjectService().persist(projectIterator.next());
        }

        @NotNull final Iterator<Task> taskIterator = objectMapper.readerFor(Task.class).readValues(new File("src/main/files/TaskFile.json"));
        while (taskIterator.hasNext()) {
            serviceLocator.getTaskService().persist(taskIterator.next());
        }

        @NotNull final Iterator<User> userIterator = objectMapper.readerFor(User.class).readValues(new File("src/main/files/UserFile.json"));
        while (userIterator.hasNext()) {
            serviceLocator.getUserService().persist(userIterator.next());
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
