package ru.trifonov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.wrapper.ProjectWrapper;
import ru.trifonov.tm.wrapper.TaskWrapper;
import ru.trifonov.tm.wrapper.UserWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class DomainSaveJaxbXML extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "save-jax-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": save domain to xml with jax-b";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN JAX-B XML]");
        @NotNull final JAXBContext projectContext = JAXBContext.newInstance(ProjectWrapper.class);
        @NotNull final Marshaller projectMarshaller = projectContext.createMarshaller();
        @NotNull final ProjectWrapper projectWrapper = new ProjectWrapper();
        projectWrapper.setProjectList(serviceLocator.getProjectService().findAll()) ;
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        projectMarshaller.marshal(projectWrapper, new File("src/main/files/ProjectFile.xml"));

        @NotNull final JAXBContext taskContext = JAXBContext.newInstance(TaskWrapper.class);
        @NotNull final Marshaller taskMarshaller = taskContext.createMarshaller();
        @NotNull final TaskWrapper taskWrapper = new TaskWrapper();
        taskWrapper.setTaskList(serviceLocator.getTaskService().findAll());
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        taskMarshaller.marshal(taskWrapper, new File("src/main/files/TaskFile.xml"));

        @NotNull final JAXBContext userContext = JAXBContext.newInstance(UserWrapper.class);
        @NotNull final Marshaller userMarshaller = userContext.createMarshaller();
        @NotNull final UserWrapper userWrapper = new UserWrapper();
        userWrapper.setUserList(serviceLocator.getUserService().findAll());
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        userMarshaller.marshal(userWrapper, new File("src/main/files/UserFile.xml"));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
