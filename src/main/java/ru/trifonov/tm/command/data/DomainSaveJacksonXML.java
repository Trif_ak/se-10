package ru.trifonov.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

import java.io.FileOutputStream;

public final class DomainSaveJacksonXML extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "save-jac-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": save domain to xml with jackson";
    }

    @Override
    public void execute() throws Exception {
        try(@NotNull final FileOutputStream userOut = new FileOutputStream("src/main/files/UserFile.xml");
            @NotNull final FileOutputStream taskOut = new FileOutputStream("src/main/files/TaskFile.xml");
            @NotNull final FileOutputStream projectOut = new FileOutputStream("src/main/files/ProjectFile.xml")
        ){
            System.out.println("[DOMAIN JACKSON XML]");
            @NotNull final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
            @NotNull final String userJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getUserService().findAll());
            userOut.write(userJackson.getBytes());
            userOut.flush();
            @NotNull final String projectJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getProjectService().findAll());
            projectOut.write(projectJackson.getBytes());
            projectOut.flush();
            @NotNull final String taskJackson = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getTaskService().findAll());
            taskOut.write(taskJackson.getBytes());
            taskOut.flush();
            System.out.println("[OK]");
        }
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
