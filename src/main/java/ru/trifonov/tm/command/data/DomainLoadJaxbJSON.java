package ru.trifonov.tm.command.data;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.wrapper.ProjectWrapper;
import ru.trifonov.tm.wrapper.TaskWrapper;
import ru.trifonov.tm.wrapper.UserWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DomainLoadJaxbJSON extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "load-jax-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": load domain to json with jax-b";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN JAX-B XML]");
        @NotNull final JAXBContext projectContext = JAXBContext.newInstance(ProjectWrapper.class);
        @NotNull final Unmarshaller projectUnmarshaller = projectContext.createUnmarshaller();
        projectUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        projectUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final ProjectWrapper projectWrapper = (ProjectWrapper) projectUnmarshaller.unmarshal(new File("src/main/files/ProjectFile.json"));
        for (@NotNull final Project project : projectWrapper.getProjectList()) serviceLocator.getProjectService().persist(project);

        @NotNull final JAXBContext taskContext = JAXBContext.newInstance(TaskWrapper.class);
        @NotNull final Unmarshaller taskUnmarshaller = taskContext.createUnmarshaller();
        taskUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        taskUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final TaskWrapper taskWrapper = (TaskWrapper) taskUnmarshaller.unmarshal(new File("src/main/files/TaskFile.json"));
        for (@NotNull final Task task : taskWrapper.getTaskList()) serviceLocator.getTaskService().persist(task);

        @NotNull final JAXBContext userContext = JAXBContext.newInstance(UserWrapper.class);
        @NotNull final Unmarshaller userUnmarshaller = userContext.createUnmarshaller();
        userUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        userUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final UserWrapper userWrapper = (UserWrapper) userUnmarshaller.unmarshal(new File("src/main/files/UserFile.json"));
        for (@NotNull final User user : userWrapper.getUserList()) serviceLocator.getUserService().persist(user);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
