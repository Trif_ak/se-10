package ru.trifonov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public final class DomainSerializable extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "ser-domain";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": serializable domain";
    }

    @Override
    public void execute() throws Exception {
        try (@NotNull final FileOutputStream projectOut = new FileOutputStream("src/main/files/ProjectFile");
             @NotNull final FileOutputStream taskOut = new FileOutputStream("src/main/files/TaskFile");
             @NotNull final FileOutputStream userOut = new FileOutputStream("src/main/files/UserFile")
        ){
            System.out.println("[DOMAIN SERIALIZABLE]");
            @NotNull final ObjectOutputStream projectObjectOut = new ObjectOutputStream(projectOut);
            projectObjectOut.writeObject(serviceLocator.getProjectService().findAll());
            @NotNull final ObjectOutputStream taskObjectOut = new ObjectOutputStream(taskOut);
            taskObjectOut.writeObject(serviceLocator.getTaskService().findAll());
            @NotNull final ObjectOutputStream userObjectOut = new ObjectOutputStream(userOut);
            userObjectOut.writeObject(serviceLocator.getUserService().findAll());
            System.out.println("[OK]");
        }
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}