package ru.trifonov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.enumerate.RoleType;

import java.text.SimpleDateFormat;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract RoleType[] roleType();
}
