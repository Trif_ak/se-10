package ru.trifonov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.User;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "userWrapper")
@XmlAccessorType(XmlAccessType.FIELD)
public final class UserWrapper {
    @XmlElement(name = "userList")
    @Nullable private List<User> userList = null;

    @Nullable
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(@Nullable final List<User> userList) {
        this.userList = userList;
    }
}
