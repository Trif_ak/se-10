package ru.trifonov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.Task;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "taskWrapper")
@XmlAccessorType(XmlAccessType.FIELD)
public final class TaskWrapper {
    @XmlElement(name = "taskList")
    @Nullable private List<Task> taskList = null;

    @Nullable
    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(@Nullable final List<Task> taskList) {
        this.taskList = taskList;
    }
}
