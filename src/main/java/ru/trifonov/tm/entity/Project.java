package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement(name = "project")
public final class Project extends AbstractComparableEntity implements Comparable<Project>, Serializable {
    public Project(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public int compareTo(@NotNull Project project) {
        return this.getId().compareTo(project.getId());
    }
}