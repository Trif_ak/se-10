package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.CurrentStatus;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractComparableEntity {
    @NotNull
    protected String name = "";
    @NotNull
    protected String id = "";
    @NotNull
    protected String projectId = "";
    @NotNull
    protected String userId = "";
    @NotNull
    protected CurrentStatus status = CurrentStatus.PLANNED;
    @NotNull
    protected String description = "";
    @NotNull
    protected Date createDate = new Date();
    @NotNull
    protected Date beginDate = new Date();
    @NotNull
    protected Date endDate = new Date();
    @NotNull
    protected final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyy");

    protected AbstractComparableEntity(@NotNull String name, @NotNull String id, @NotNull String userId, @NotNull CurrentStatus status, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.status = status;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    protected AbstractComparableEntity(@NotNull String name, @NotNull String id, @NotNull String projectId, @NotNull String userId, @NotNull CurrentStatus status, @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.status = status;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  STATUS " + status.getCurrentStatus() +
                "  PROJECT BEGIN DATE " + dateFormat.format(beginDate) +
                "  PROJECT END DATE " + dateFormat.format(endDate);
    }
}
