package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement(name = "task")
public final class Task extends AbstractComparableEntity  implements Comparable<Task>, Serializable {
    public Task(@NotNull String name, @NotNull String id, @NotNull String projectId, @NotNull String userId, @NotNull String description, @NotNull Date beginDate, Date endDate) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  STATUS " + status.getCurrentStatus() +
                "  PROJECT BEGIN DATE " + dateFormat.format(beginDate) +
                "  PROJECT END DATE " + dateFormat.format(endDate);
    }

    @Override
    public int compareTo(@NotNull Task task) {
        return this.getId().compareTo(task.getId());
    }
}
