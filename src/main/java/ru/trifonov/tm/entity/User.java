package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement(name = "user")
public final class User implements Serializable {
    @NotNull
    private String id = "";
    @NotNull
    private String login = "";
    @NotNull
    private String passwordMD5 = "";
    private RoleType roleType;

    public User(@NotNull String id, @NotNull String login, @NotNull String passwordMD5, RoleType roleType) {
        this.id = id;
        this.login = login;
        this.passwordMD5 = passwordMD5;
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  NAME " + login +
                "  PASSWORD " + passwordMD5 +
                "  USER'S ROLE " + roleType.getRoleType();
    }
}
