package ru.trifonov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.AbstractComparableEntity;

import java.util.Comparator;

public final class StatusComparator implements Comparator<AbstractComparableEntity> {
    @Override
    public int compare(@NotNull final AbstractComparableEntity o1, @NotNull final AbstractComparableEntity o2) {
        return o1.getStatus().compareTo(o2.getStatus());
    }
}
